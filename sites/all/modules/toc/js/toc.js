(function ($) {

Drupal.behaviors.tocLoader = {
  attach: function (context, settings) {
    $(Drupal.settings.toc_scope, context).once('toc-scope', function() {
		$('.node-toc-content h2').add('.node-toc-content h3').each(function() {
			$(this).attr('id', translit($(this).text()));
		})
		var toc = $('<ol class="art-content-list" id="toc"></ul>');

      toc = toc.tableOfContents(this, {
        startLevel: Drupal.settings.toc_start_level,
        depth: Drupal.settings.toc_depth,
        topLinks: Drupal.settings.toc_topLinks
      });

      if ($('li', toc).length > 0) {
        $(Drupal.settings.toc_dest).prepend(toc);
        $('#toc').localScroll();
        // For toplinks.
        $('.content').localScroll();
      }else {
		  $('.node-toc-wrap').hide();
	  }
    });
  }
};

function translit(text){
	// Символ, на который будут заменяться все спецсимволы
	var space = '-';
	// Массив для транслитерации
	var transl = {
	'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh', 
	'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
	'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
	'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh','ъ': space, 'ы': 'y', 'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya',
	'А': 'a', 'Б': 'b', 'В': 'v', 'Г': 'g', 'Д': 'd', 'Е': 'e', 'Ё': 'e', 'Ж': 'zh', 
	'З': 'z', 'И': 'i', 'Й': 'j', 'К': 'k', 'Л': 'l', 'М': 'm', 'Н': 'n',
	'О': 'o', 'П': 'p', 'Р': 'r','С': 's', 'Т': 't', 'У': 'u', 'Ф': 'f', 'Х': 'h',
	'Ц': 'c', 'Ч': 'ch', 'Ш': 'sh', 'Щ': 'sh', 'Ы': 'y', 'Э': 'e', 'Ю': 'yu', 'Я': 'ya',
	' ': space, '_': space, '`': space, '~': space, '!': space, '@': space,
	'#': space, '$': space, '%': space, '^': space, '&': space, '*': space, 
	'(': space, ')': space,'-': space, '\=': space, '+': space, '[': space, 
	']': space, '\\': space, '|': space, '/': space,'.': space, ',': space,
	'{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
	'?': space, '<': space, '>': space, '№':space
	}
					
	var result = '';
	var curent_sim = '';
					
	for(var i = 0; i < text.length; i++) {
		// Если символ найден в массиве то меняем его
		if(transl[text[i]] != undefined) {
			if(curent_sim != transl[text[i]] || curent_sim != space){
				result += transl[text[i]];
				curent_sim = transl[text[i]];
															}                                                                            
		}
		// Если нет, то оставляем так как есть
		else {
			result += text[i];
			curent_sim = text[i];
		}                              
	}          
					
	return $.trim(result);               
					
	// Выводим результат 
// 		$('#alias').val(result); 
	
}

function TrimStr(s) {
	s = s.replace(/^-/, '');
	return s.replace(/-$/, '');
}

})(jQuery);
