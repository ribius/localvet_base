<?php

/**
 * @file
 * widget.tpl.php
 *
 * UpDown widget theme for Vote Up/Down
 */
?>
<div class="vud-widget vud-widget-updown" id="<?php print $id; ?>">
	<?php if ($show_links): ?>
		<?php if ($show_up_as_link): ?>
			<a href="<?php print $link_up; ?>" rel="nofollow" class="<?php print $link_class_up; ?> rate-up"></a>
		<?php else: ?>
			<span class="<?php print $class_up; ?> rate-up"></span>
		<?php endif; ?>
		<?php if ($show_down_as_link): ?>
			<a href="<?php print $link_down; ?>" rel="nofollow" class="<?php print $link_class_down; ?> rate-down"></a>
		<?php else: ?>
			<span class="<?php print $class_down; ?> rate-down"></span>
		<?php endif; ?>
	<?php endif; ?>
	<span class="rate-value value-plus"><?php print $unsigned_points; ?></span>
</div>
