(function ($) {
  Drupal.behaviors.binet_perelink = {
    attach: function(context, settings) {
      $('#perelink li').once('perelink').on('click', function () {
         window.location = $(this).find('a').attr('href'); 
         return false;
      });
    }
  };
})(jQuery);