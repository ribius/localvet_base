(function ($) {
	Drupal.behaviors.localvet_map = {
		attach: function (context, settings) {
// 			alert('localvet_map');
			console.log(settings);
			if(settings.localvet_map) {
// 				alert('map_settings');
				$("#firms-map").css('background', 'url(/sites/all/modules/localvet/localvet_map/ajax-loader.gif) no-repeat center center');
				st = Date.now() / 1000;
				var maps_markers = {};
				var page_ready = 0;
				for(i = 0; i < settings.localvet_map.total_page; i++) {
					url = '/firms/' + settings.localvet_map.term_tid + '/' + i + '/nojs/map';
					console.log(url);
					$.getJSON(url, function(data) 
					{ 
						console.log(data);
						//maps_markers = maps_markers.concat(JSON.parse(data[1].data));
						Object.assign(maps_markers, JSON.parse(data[1].data));
						page_ready++;
						if(page_ready == settings.localvet_map.total_page) {
// 							alert('ready');
							console.log(maps_markers);
							t = (Date.now() / 1000) - st;
// 							alert(t);
							initMap(maps_markers);
						}
					}).done(function() { console.log( "success" ); }).fail(function() { console.log( "error" ); });
				}
				//alert(page_ready);
			}
		}
	}
	
	function initMap(maps_markers) {
		var map = new google.maps.Map(document.getElementById('firms-map'), {
			zoom: 3,
			center: {lat: -28.024, lng: 140.887},
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: []
		});
// 		map.infowindow = new google.maps.InfoWindow();

		bounds  = new google.maps.LatLngBounds();
		var markers = Object.keys(maps_markers).map(function(key, i) {
// 			console.log(parseFloat(data.field_map_lat));
			data = maps_markers[key]
			marker = new google.maps.Marker({
				position: {lat: parseFloat(data.field_map_lat), lng: parseFloat(data.field_map_lon)},
				map: map,
				title: data.node_title,
				icon: "/sites/all/modules/localvet/localvet_map/pic-map.png"
			});
			infowindow = new google.maps.InfoWindow(/*{
				content: '<div class="marcer-content"><a href="/node/' + parseInt(data.nid) + '" class="marker-link">' + data.node_title + '</a>'
			}*/);
			content = '<div class="marcer-content"><a href="/node/' + parseInt(data.nid) + '" class="marker-link">' + data.node_title + '</a>';
			google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
				return function() {
					infowindow.setContent(content);
					infowindow.open(map,marker);
				};
			})(marker,content,infowindow));
// 			marker.addListener('click', function() {
// 				infowindow.open(map, marker);
// 			});
			loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
			bounds.extend(loc);
			return marker;
		});
		
		map.fitBounds(bounds);       //auto-zoom
		map.panToBounds(bounds);     //auto-center

		// Add a marker clusterer to manage the markers.
		var markerCluster = new MarkerClusterer(map, markers, {
			imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
		});
		$("#firms-map").css('background', null);
	}
	
})(jQuery)