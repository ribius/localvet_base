<div class="search-row item-name">
	<?php print drupal_render($form['title']); ?>
</div>
<div class="search-colums">
	<div class="search-col">
		<div class="search-row">
			<?php print drupal_render($form['region']); ?>
		</div>
		<div class="search-row">
			<?php print drupal_render($form['town']); ?>
		</div>
	</div>
		
	<div class="search-col">
		<div class="search-row">
			<?php print drupal_render($form['type']); ?>
		</div>
		
		<div class="search-row">
			<?php print drupal_render($form['product']); ?>
		</div>
	</div>
</div>

<div class="form-actions">
	<div class="reset"><a href="/spravochnik" class="reset-btn">Сбросить фильтр</a></div>
	<?php print drupal_render($form['form_build_id']) ?>
	<?php print drupal_render($form['form_id']) ?>
	<?php print drupal_render($form['submit']) ?>
	<?php print drupal_render_children($form) ?>
</div>