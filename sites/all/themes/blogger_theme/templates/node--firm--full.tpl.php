<?php
/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> firm-full"<?php print $attributes; ?>>

	<?php print render($title_prefix); ?>
	<?php print render($title_suffix); ?>
	<?php //dpm($content); ?>
	<?php if(isset($content['field_rate']) && $content['field_rate']['#items'][0]['count'] > 0): ?>
		<div class="firm-rate"><?php print render($content['field_rate']); ?></div>
	<?php endif; ?>
	<div class="row">
		<?php print block_render('block', 8); ?>
	</div>
	
	<div class="row">
		<div class="logo">
			<?php print render($content['field_image']); ?>
		</div>
		<div class="map">
			<?php print views_embed_view('map', 'block', $node->nid); ?>
		</div>
	</div>
	
	<?php print render($content['field_contacts']); ?>
	
	<?php if(isset($content['field_site'])): ?>
		<div class="firm-contacts-item firm-site">
			<label>Сайт:</label>
			<?php if(isset($content['field_pay']) && render($content['field_pay']) != FALSE): ?>
				<?php print render($content['field_site']); ?>
			<?php else: ?>
				<?php $site = field_view_field('node', $node, 'field_site', array('label' => 'hidden', 'type' => 'link_host')); print render($site); ?>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<?php if(isset($content['field_email'])): ?>
		<div class="firm-contacts-item firm-email">
			<label>Почта:</label>
			<?php print render($content['field_email']); ?>
	<?php endif; ?>
	<?php if(isset($content['field_undeground'])): ?>
		<div class="row">
			<div class="firm-contacts-item firm-undeground">
				<label>Ближайшие станции метро:</label><?php print render($content['field_undeground']); ?>
			</div>
		</div>
	<?php endif; ?>
	
	<hr>
	
	<?php if(isset($content['field_desc'])): ?>
		<div class="firm-desc"><?php print render($content['field_desc']); ?></div>
	<?php endif; ?>
	
	<?php if(isset($content['field_option'])): ?>
		<div class="row">
			<div class="firm-options">
				<?php print render($content['field_option']); ?>
			</div>
		</div>	
	<?php endif; ?>
	
	<div class="row">
		<?php print block_render('block', 9); ?>
	</div>
	
	<hr>
	
	<?php print views_embed_view('tiu', 'block'); ?>
	
	<div class="share">
		<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
		<script src="//yastatic.net/share2/share.js"></script>
		<span>Поделиться в социальных сетях:</span>
		<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter"></div>
	</div>
	<div class="my-organization">
		<?php print l('Это Ваша организация?', 'node/398', array('attributes' => array('class' => 'is-my'))); ?>
		<?php print l('Сообщить об ошибке', 'node/397', array('query' => array('from' => $node->nid), 'attributes' => array('class' => 'find-error'))); ?>
		<!--<span class="is-my">Это Ваша организация?</span>-->
		<!--<a href="#" class="find-error">Сообщить об ошибке</a>-->
	</div>
	
	<?php print render($content['comments']); ?>
	
	<hr>
	
	<?php print views_embed_view('firms', 'block_2'); ?>
	
	<!-- Yandex.RTB R-A-224667-8 -->
<div id="yandex_rtb_R-A-224667-8"></div>
<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "R-A-224667-8",
                renderTo: "yandex_rtb_R-A-224667-8",
                horizontalAlign: false,
                async: true
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>
	
</div>