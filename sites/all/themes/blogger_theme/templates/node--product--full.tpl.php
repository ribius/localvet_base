<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php $user_author = user_load($node->uid); ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> product-full clearfix"<?php print $attributes; ?>>

	<?php print render($title_prefix); ?>
	<?php print render($title_suffix); ?>
	<?php $firm = $content['field_firm']['#items'][0]['entity']; ?>
	<div class="row">
		<?php print block_render('block', 11); ?>
	</div>
	
	<div class="product-top">
		<?php if(isset($content['field_img'])): ?>
			<div class="top-col">
				<div class="tiu-foto tiu-foto-magnificpopup"><?php print render($content['field_img']); ?></div>
				<div class="tiu-foto-mini"><?php $img_thumb = field_view_field('node', $node, 'field_img', array('label' => 'hidden', 'type' => 'image', array('image_style' => 'tiu_mini', 'image_link' => ''))); print render($img_thumb); ?></div>
			</div>
		<?php endif; ?>
		<div class="top-col">
			<p>Организация: <?php print render($content['field_firm']); ?></p>
			<?php if(isset($content['field_rubric_tiu'])): ?>
				<p>Рубрика: <?php print render($content['field_rubric_tiu']); ?></p>
			<?php endif; ?>
			<p>Отзывы: <?php print l($firm->comment_count, 'node/' . $firm->nid, array('fragment' => 'comment')); ?></p>
			<?php if(isset($content['field_price'])): ?>
				<p>Цена: <?php print render($content['field_price']); ?></p>
			<?php endif; ?>
			<?php if(!in_array('administrator', $user_author->roles)): ?>
				<?php //print l('Заказать', 'node/399', array('query' => array('from' => $node->nid), 'attributes' => array('class' => 'btn'))); ?>
				<?php print l('Заказать', 'node-ajax/nojs/399', array(
					'query' => array('nid' => $node->nid),
					'attributes' => array( 'class' => array('btn order-link use-ajax')),
				)); ?>
			<?php endif; ?>
		</div>
	</div>
	
	<?php if(isset($content['field_desc'])): ?>
		<?php print render($content['field_desc']); ?>
	<?php endif; ?>
	
	<div class="row">
		<?php print block_render('block', 13); ?>
	</div>
	
	<div class="row share">
		<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
		<script src="//yastatic.net/share2/share.js"></script>
		<span>Поделиться в социальных сетях:</span>
		<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter"></div>
	</div>
	
	<hr>
	
	<?php print views_embed_view('tiu', 'block_1', $node->field_firm[LANGUAGE_NONE][0]['target_id']); ?>
	
	<?php print views_embed_view('tiu', 'block_2', $node->nid, $node->field_firm[LANGUAGE_NONE][0]['target_id']); ?>
	
	<?php print block_render('block', 12); ?>
	
	<div id="popup-order">
		<div class="modal-content"></div>
	</div>

</div>
