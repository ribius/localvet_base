(function($) {
	"use strict";
	$(document).ready(function(){
		// Main menu superfish
		$('#main-menu > ul').superfish({
			delay: 200,
			animation: {opacity:'show', height:'show'},
			speed: 'fast',
			cssArrows: false,
			disableHI: true
		});
		
		// Mobile Menu
		$('#navigation-toggle').sidr({
			name: 'sidr-main',
			source: '#sidr-close, #site-navigation',
			side: 'left',
			displace: false
		});
		$(".sidr-class-toggle-sidr-close").click( function() {
			$.sidr('close', 'sidr-main');
			return false;
		});
		
		$('.comment-form .form-type-item a').click(function() {
			return false;
		})
		
		$('select, input[type=file]').not('.form-type-fivestar select').styler();
		
		$('.tiu-foto-mini img').click(function() {
			var i = $('.tiu-foto-mini img').index($(this));
			$('.tiu-foto a').hide();
			$('.tiu-foto a').eq(i).show();
		});
		
		$('.tiu-foto-magnificpopup a').magnificPopup({
			type:'image',
			gallery: {
				enabled: true,
				tCounter: '<span class="mfp-counter">%curr% из %total%</span>',
				tPrev: 'След.',
				tNext: 'Пред.'
			},
			image: {
				titleSrc: 'title'
			}
		});
		
		directFixed();
		
		$('.field-name-field-site .form-type-link-field .link-field-url .form-item').append($('.field-name-field-site .form-type-link-field > .description').clone());
		$('.field-name-field-option table.field-multiple-table th').append($('.field-name-field-option .description').clone());
		
	}); // End doc ready
	
	Drupal.behaviors.bf = {
		attach: function (context, settings) {
			$('select, input[type=file]').not('.form-type-fivestar select').styler();
			
			if($('#node-399', context).length) {
				$('input, select').styler();
				$.magnificPopup.open({
					items: {
						src: '#popup-order'
					},
					type: 'inline'
				});
			}
			
			$('.form-type-hierarchical-select').each(function() {
				if(!$(this).find('.hierarchical-select .description').length) {
					$(this).find('.description').clone().insertAfter($(this).find('.hierarchical-select .selects'));
				}
			});
		}
	}
	
	function directFixed() {
// 		alert($('#content').height());
// 		alert($('#secondary').height());
		if ($(document).width() >= 767 && $('#content').height() >= 1250) {
			var offset = $('#block-block-1').offset();
// 			alert('fromTop' + offset.top);
			$(window).scroll(function() {
// 				alert(fromTop);
				if($(this).scrollTop() > offset.top ) {
					$('#block-block-1').addClass('block-fixed');
				}else {
					$('#block-block-1').removeClass('block-fixed');
				}
			});
		}
	}
	
})(jQuery);