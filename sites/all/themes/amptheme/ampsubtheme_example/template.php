<?php

/**
 * Preprocess all templates.
 */
function ampsubtheme_example_preprocess(&$vars, $hook) {
  $vars['ampsubtheme_path_file'] = DRUPAL_ROOT . '/' . drupal_get_path('theme', 'ampsubtheme_example');
}

/**
 * Implements hook_preprocess_HOOK() for HTML document templates.
 *
 * Example of a preprocess hook for a subtheme that could be used to change
 * variables in templates in order to support custom styling of AMP pages.
 */
function ampsubtheme_example_preprocess_html(&$variables) {

}

function ampsubtheme_example_preprocess_node(&$variables) {
	$variables['view_mode'] = $variables['elements']['#view_mode'];
	$variables['node'] = $variables['elements']['#node'];
	$node = $variables['node'];
	
	$variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $variables['view_mode'];
	$variables['theme_hook_suggestions'][] = 'node__' . $node->nid . '__' . $variables['view_mode'];
}

function amp_block_render($module_name, $block_delta) {
	$block = block_load($module_name, $block_delta);
	$block = _block_render_blocks(array($block));
	$block_build = _block_get_renderable_array($block);
	return drupal_render($block_build);
}