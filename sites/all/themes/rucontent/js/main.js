(function ($) {
	$(function(){

// 		$('input[placeholder], textarea[placeholder]').placeholder();


		$('.open-modal').magnificPopup({
			type:'inline',
			midClick: true
		});
		$('.card-prod .owl-slider-main a').magnificPopup({
			type:'image',
			midClick: true
		});



		// add-open-class
		$('.menu-icon-wrap').click(function(){
		if($(this).parent().is('.menu-mobile-open')){
			$(this).parent().removeClass('menu-mobile-open');
		}else{
			$(this).parent().addClass('menu-mobile-open');
		}
		});

		// add-open-class
		$('.menu-dd-open-link').click(function(){
		if($(this).parent().is('.menu-dd-open')){
			$(this).parent().removeClass('menu-dd-open');
		}else{
			$(this).parent().addClass('menu-dd-open');
		}
		});

		// add-open-class
		$('.head-search .icon').click(function(){
		if($(this).parent().is('.open')){
			$(this).parent().removeClass('open');
		}else{
			$(this).parent().addClass('open');
		}
		});


		$(document).bind('click touchstart', function(e) {
			var $clicked = $(e.target);
			if (! $clicked.parents().hasClass("head-search")){
			$(".head-search").removeClass("open");
			}
		});
		
		$('.user-button a').eq(0).addClass('active');
		$('.user-button a').click(function() {
			$('.user-tab').hide();
			$('.user-button a').removeClass('active');
			$(this).addClass('active');
			$($(this).attr('href')).show();
			return false;
		})


	});



	$(window).load(function() {



		//----------------------------------------------------
		// owl carousel thumb
		//

		var sync1 = $(".owl-slider-main");
		var sync2 = $(".owl-thumbs");
		var slidesPerPage = 4; //globaly define number of elements per page
		var syncedSecondary = true;

		sync1.owlCarousel({
			items : 1,
			slideSpeed : 2000,
			nav: false,
			autoplay: false,
			dots: false,
			loop: true,
			responsiveRefreshRate : 200,
			navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
		}).on('changed.owl.carousel', syncPosition);

		sync2
			.on('initialized.owl.carousel', function () {
			sync2.find(".owl-item").eq(0).addClass("current");
			})
			.owlCarousel({
			items : slidesPerPage,
			dots: false,
			nav: false,
			smartSpeed: 200,
			slideSpeed : 500,
			slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
			responsiveRefreshRate : 100
		}).on('changed.owl.carousel', syncPosition2);

		function syncPosition(el) {
			//if you set loop to false, you have to restore this next line
			//var current = el.item.index;
			
			//if you disable loop you have to comment this block
			var count = el.item.count-1;
			var current = Math.round(el.item.index - (el.item.count/2) - .5);
			
			if(current < 0) {
			current = count;
			}
			if(current > count) {
			current = 0;
			}
			
			//end block

			sync2
			.find(".owl-item")
			.removeClass("current")
			.eq(current)
			.addClass("current");
			var onscreen = sync2.find('.owl-item.active').length - 1;
			var start = sync2.find('.owl-item.active').first().index();
			var end = sync2.find('.owl-item.active').last().index();
			
			if (current > end) {
			sync2.data('owl.carousel').to(current, 100, true);
			}
			if (current < start) {
			sync2.data('owl.carousel').to(current - onscreen, 100, true);
			}
		}
		
		function syncPosition2(el) {
			if(syncedSecondary) {
			var number = el.item.index;
			sync1.data('owl.carousel').to(number, 100, true);
			}
		}
		
		sync2.on("click", ".owl-item", function(e){
			e.preventDefault();
			var number = $(this).index();
			sync1.data('owl.carousel').to(number, 300, true);
		});

		//
		// end owl carousel thumb
		//----------------------------------------------------
		
		$('.field-name-field-site .form-type-link-field .link-field-url .form-item').append($('.field-name-field-site .form-type-link-field > .description').clone());
		$('.h-search-field').attr('placeholder', 'Введите запрос...')
		
		directFixed();
		
		fontLoader.loadFonts();
	
	});
	
	Drupal.behaviors.localvet = {
		attach: function (context, settings) {
			//form elements
			if($('.node-faq-form').length) {
				jcf.setOptions('File', {
					buttonText: 'обзор',
					placeholderText: 'Изображение...'
				});
			}else {
				jcf.setOptions('File', {
					buttonText: 'обзор',
					placeholderText: 'Логотип...'
				});
			}
			
			jcf.setOptions('Select', {
				wrapNative: false,
				multipleCompactStyle: true,
				maxVisibleItems: 10,
				useCustomScroll: false
			});
			
			jcf.replaceAll();
			
			if($('#node-399', context).length) {
				$.magnificPopup.open({
					items: {
						src: '#modalCard'
					},
					type: 'inline'
				});
			}
			
// 			if($('body', context).length == 0) {
				$('.main-card-wrapp').masonry({
					itemSelector: '.m-card-item'
				});
// 			}
			
		}
	}
	
	function directFixed() {
// 		alert($('#content').height());
// 		alert($('#secondary').height());
		if ($(document).width() >= 767 && $('.main-column').height() >= $('.region-right').height()) {
			var offset = $('#block-block-1').offset();
// 			alert('fromTop' + offset.top);
			$(window).scroll(function() {
// 				alert(fromTop);
				if($(this).scrollTop() > offset.top ) {
					$('#block-block-1').addClass('block-fixed');
				}else {
					$('#block-block-1').removeClass('block-fixed');
				}
			});//http://joxi.ru/KAgYgMLcgeYW8m
		}
	}
	
	var fontLoader = new FontLoader(["NEXTART_Bold"], {
		"fontLoaded": function(font) {
			$('.main-card-wrapp').masonry({
				itemSelector: '.m-card-item'
			});
		}
	}, 300);
	
})(jQuery)