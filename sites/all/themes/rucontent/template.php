<?php

function localvet_preprocess_page(&$variables) {
	drupal_add_js('jQuery.extend(Drupal.settings, { "pathToTheme": "' . path_to_theme() . '" });', 'inline');
	
// 	$contact_node_nid = variable_get('contact_node_nid', 1);
// 	$contact_node = node_load($contact_node_nid);
// 	$phone = field_get_items('node', $contact_node, 'field_phone');
// 	$phone = field_view_value('node', $contact_node, 'field_phone', $phone[0], 'full');
// 	$phone = render($phone);
// 	$variables['phone'] = l($phone, 'tel:+' . str_replace(array('-', ' ', '+', '(', ')'), '', $phone));
// 	$work_time = field_get_items('node', $contact_node, 'field_work_time');
// 	$work_time = field_view_value('node', $contact_node, 'field_work_time', $work_time[0], 'full');
// 	$variables['work_time'] = render($work_time);
	$variables['main_menu'] = menu_tree('main-menu');
	$variables['firms_filter'] = block_render('localvet_filters', 'firms_filter_right');
// 	$variables['menu_footer_catalog_1'] = menu_tree('menu-footer-catalog-1');
// 	$variables['menu_footer_catalog_2'] = menu_tree('menu-footer-catalog-2');
// 	$variables['menu_footer_catalog_3'] = menu_tree('menu-footer-catalog-3');
	
	$variables['head_bg'] = $variables['base_path'] . $variables['directory'] . '/img/bg1.jpg';
	if($node = menu_get_object()) {
		$variables['theme_hook_suggestions'][] = 'page__node__' . $node->type;
// 		if($node->type == 'faq') {
// 			drupal_set_title($node->field_short_desc[LANGUAGE_NONE][0]['value']);
// 		}
	}
	
	$args = arg();
	
	if($term = menu_get_object('taxonomy_term', 2)) {
		if(in_array($term->vocabulary_machine_name, array('rubrics_firms', 'rubrics_tiu', 'undeground'))) {
			$html_obj = new simple_html_dom();
// 			dpm($variables['page']);
			$main_key = (isset($variables['page']['content']['system_main']['main'])) ? 'main' : 'content';
			$html_obj->load($variables['page']['content']['system_main'][$main_key]['#markup']);
			$term_desc = ($html_obj->find('div[class=pane-term-desc]', 0)) ? $html_obj->find('div[class=pane-term-desc]', 0)->innertext : '';
			if($term_desc != '') {
				$variables['term_desc'] = "<div class='aside-light-bl-wrapp'>\n<div class='light-bl'>\n<span class='site-h3-tt line-tt'>Описание</span>\n{$term_desc}\n</div>\n</div>\n";
				$variables['page']['content']['system_main'][$main_key]['#markup'] = str_replace($term_desc, '', $variables['page']['content']['system_main'][$main_key]['#markup']);
				$html_obj->find('div[class=pane-term-desc]', 0)->outertext = '';
				$variables['page']['content']['system_main'][$main_key]['#markup'] = $html_obj->save();
			}
		}
		//dpm($variables);
	}
	if($args[0] == 'user' && !isset($args[1]) && !$variables['logged_in']) {
		drupal_goto('user/login');
	}
	if($args[0] == 'user') {
		$variables['theme_hook_suggestions'][] = 'page__user';
	}
	if($args[0] == 'user' && $args[1] == 'register') {
		drupal_set_title('Регистрация');
	}
	if($args[2] == 'faq') {
		$variables['theme_hook_suggestions'][] = 'page';
	}
	$variables['args'] = $args;

}

function localvet_preprocess_taxonomy_term(&$variables) {
	$variables['view_mode'] = $variables['elements']['#view_mode'];
	$variables['term'] = $variables['elements']['#term'];
	$term = $variables['term'];
	$variables['theme_hook_suggestions'][] = 'taxonomy_term__' . $variables['elements']['#bundle'] . '__' . $variables['view_mode'];
	$variables['base_path'] = base_path();
	$variables['directory'] = path_to_theme();
}

function localvet_preprocess_node(&$variables) {
	$variables['view_mode'] = $variables['elements']['#view_mode'];
	$variables['node'] = $variables['elements']['#node'];
	$node = $variables['node'];
	
	if($node->type == 'article') {
		if(isset($node->metatags['ru']['keywords'])) {
			$variables['keywords'] = $node->metatags['ru']['keywords']['value'];
		}
	}
	
	$variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $variables['view_mode'];
	$variables['theme_hook_suggestions'][] = 'node__' . $node->nid . '__' . $variables['view_mode'];
	
	$variables['base_path'] = base_path();
	$variables['directory'] = path_to_theme();
	
}

function localvet_preprocess_comment_wrapper(&$variables) {
  // Provide contextual information.
  $variables['node'] = $variables['content']['#node'];
  $variables['theme_hook_suggestions'][] = 'comment_wrapper__' . $variables['node']->type;
}

function localvet_preprocess_comment(&$variables) {
	$comment = $variables['elements']['#comment'];
	$node = $variables['elements']['#node'];
	$variables['theme_hook_suggestions'][] = 'comment__' . $variables['elements']['#node']->type;
	//dpm($variables);
	if(isset($node->view)) {
		if($node->view->name == 'comments' && $node->view->current_display == 'block_1') {
			$variables['theme_hook_suggestions'][] = 'comment__teaser';
		}
	}
	if(isset($comment->view)) {
		$view = $comment->view;
		if($view->name == 'comments' && $view->current_display == 'block_2') {
			$variables['theme_hook_suggestions'][] = 'comment__teaser_2';
		}
	}
	$variables['base_path'] = base_path();
	$variables['directory'] = path_to_theme();
//   dpm($comment);
//   dpm($node);
//   dpm($content);
}

function shtory_preprocess_panels_pane(&$vars) {
  // Add template file suggestion for content type and sub-type.
	$vars['theme_hook_suggestions'][] = 'panels_pane__pid__' . $vars['pane']->pid;
	//dpm($vars['theme_hook_suggestions']);

}

function localvet_preprocess_views_view(&$vars) {
	$vars['base_path'] = base_path();
	$vars['directory'] = path_to_theme();
}

function localvet_form_alter(&$form, &$form_state, $form_id) {
	//dpm($form_id);
	switch($form_id) {
		case 'webform_client_form_2':
		case 'webform_client_form_13':
		case 'webform_client_form_399':
		case 'webform_client_form_397':
		case 'webform_client_form_14298':
			//dpm($form);
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'][] = 'btn btn-md';
			break;
		case 'user_login':
		case 'user_login_block':
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'][] = 'btn btn-md';
			$form['actions']['submit']['#value'] = 'Войти на сайт';
			unset($form['links']);
// 			$form['name']['#attributes']['class'][] = 'input form__input';
// 			$form['pass']['#attributes']['class'][] = 'input form__input';
			//dpm($form);
			break;
		case 'user_pass':
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'][] = 'btn';
			$form['name']['#attributes']['class'][] = 'input form__input';
			break;
		case 'user_pass_reset':
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'][] = 'btn';
			break;
		case 'user_register_form':
			//dpm($form);
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'][] = 'btn btn-md';
			$form['#submit'][] = 'localvet_user_register_submit';
			break;
		case 'advpoll_choice_form':
			//dpm($form);
			$form['submit']['#type'] = 'button';
			$form['submit']['#value'] = 'Ответить';
			$form['submit']['#attributes']['class'] = array('btn', 'btn-md');
			break;
		case 'comment_node_firm_form':
		case 'comment_node_article_form':
		case 'comment_node_faq_form':
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'] = array('btn', 'btn-md');
			$form['actions']['submit']['#value'] = 'отправить';
			break;
		case 'search_block_form':
			$form['search_block_form']['#attributes']['class'] = array('h-search-field');
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#value'] = '';
			break;
		case 'search_form':
			$form['basic']['#attributes']['class'] = array('h-search-form');
			unset($form['basic']['keys']['#title']);
			$form['basic']['keys']['#attributes']['class'] = array('h-search-field');
			$form['basic']['submit']['#buttontype'] = 'button';
			$form['basic']['submit']['#value'] = '';
			break;
		case 'firm_node_form':
			$form['actions']['submit']['#value'] = 'Добавить компанию';
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes'] = array('class' => array('btn', 'btn-md'));
			$form['actions']['delete']['#value'] = 'Удалить компанию';
			$form['actions']['delete']['#buttontype'] = 'button';
			$form['actions']['delete']['#attributes'] = array('class' => array('btn', 'btn-md'));
// 			dpm($form);
			break;
		case 'product_node_form':
			$form['actions']['submit']['#value'] = 'Добавить услугу';
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes'] = array('class' => array('btn', 'btn-md'));
			$form['actions']['delete']['#value'] = 'Удалить услугу';
			$form['actions']['delete']['#buttontype'] = 'button';
			$form['actions']['delete']['#attributes'] = array('class' => array('btn', 'btn-md'));
// 			dpm($form);
			break;
		case 'faq_node_form':
			$form['actions']['submit']['#value'] = 'Задать вопрос';
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes'] = array('class' => array('btn', 'btn-md'));
			$form['actions']['delete']['#value'] = 'Удалить вопрос';
			$form['actions']['delete']['#buttontype'] = 'button';
			$form['actions']['delete']['#attributes'] = array('class' => array('btn', 'btn-md'));
// 			dpm($form);
			break;
		case 'hybridauth_additional_info_form':
			//dpm($form);
			$form['fset']['actions']['submit']['#buttontype'] = 'button';
			$form['fset']['actions']['submit']['#attributes']['class'][] = 'btn btn-md';
			break;
		case 'user_profile_form';
// 			dpm($form);
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'][] = 'btn btn-md';
			$form['account']['pass']['pass1']['#weight'] = -1;
			$form['account']['pass']['pass1']['#prefix'] = $form['account']['pass']['pass2']['#prefix'] = '<div class="form-group form-group-min">';
			$form['account']['pass']['pass1']['#suffix'] = $form['account']['pass']['pass2']['#suffix'] = '</div>';
			break;
		case 'user_pass':
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'][] = 'btn btn-md';
			break;
		case 'simplenews_confirm_removal_form':
			$form['#prefix'] = '<div class="inner-light-bl-wrapp"><div class="light-bl">';
			$form['#suffix'] = '</div></div>';
			$form['actions']['submit']['#buttontype'] = 'button';
			$form['actions']['submit']['#attributes']['class'][] = 'btn btn-md';
			break;
	}
}

function localvet_user_register_submit($form, &$form_state) {
  $form_state['redirect'] = 'user/register';
}

function localvet_button($variables) {
	$element = $variables['element'];
	$element['#attributes']['type'] = 'submit';

	element_set_attributes($element, array('id', 'name', 'value'));  

	$element['#attributes']['class'][] = 'form-' . $element['#button_type'];
	if (!empty($element['#attributes']['disabled'])) {
		$element['#attributes']['class'][] = 'form-button-disabled';
	}

	if (isset($element['#buttontype']) && $element['#buttontype'] == 'button') {
		$value = $element['#value'];
		unset($element['#attributes']['value']);
		return '<button' . drupal_attributes($element['#attributes']) . '>' . $value . '</button>';
	}
	else {
		return '<input' . drupal_attributes($element['#attributes']) . ' />';
	}
}

function localvet_breadcrumb($variables) {
	$breadcrumb = $variables['breadcrumb'];

	if (!empty($breadcrumb)) {
		// Provide a navigational heading to give context for breadcrumb links to
		// screen-reader users. Make the heading invisible with .element-invisible.
		if($args[0] == 'tiu' && $args[1] == 'company' && isset($args[2])) {
			$breadcrumb = array(
				l(t('Home'), '<front>'),
				l('Товары и услуги', 'tiu'),
			);
		}
		$breadcrumb[] = drupal_get_title();
		$output = theme('item_list', array('type' => 'ul', 'items' => $breadcrumb, 'attributes' => array('class' => array('bread-crumbs hidden-devices'))));
		return $output;
	}
}

function block_render($module_name, $block_delta) {
	$block = block_load($module_name, $block_delta);
	$block = _block_render_blocks(array($block));
	$block_build = _block_get_renderable_array($block);
	return drupal_render($block_build);
}

function localvet_preprocess_field(&$variables, $hook) {
	$element = $variables['element'];
	$variables['theme_hook_suggestions'][] = 'field__' . $element['#field_name'] . '__' . $element['#bundle'];
	$variables['theme_hook_suggestions'][] = 'field__' . $element['#field_name'] . '__' . $element['#bundle'] . '__' . $element['#view_mode'];
	//dpm($variables);
	//dpm($element);
}

function localvet_menu_tree__main_menu($variables) {
	return '<ul class="menu">' . $variables['tree'] . '</ul>';
}

function localvet_menu_link__main_menu($variables) {
	$element = $variables['element'];
// 	dpm($element);
	$sub_menu = '';
	$element['#attributes']['class'][] = 'catalog-menu__item';
	if ($element['#below']) {
		$element['#below']['#theme_wrappers'][0] = 'menu_tree__main_menu_sub';
		$sub_menu = drupal_render($element['#below']);
		$element['#localized_options']['html'] = TRUE;
		$element['#attributes']['class'][] = 'menu-drop-down';
		$output = '<span class="menu-dd-open-link"><span>' . $element['#title'] . '</span><i class="icon icon-arrow-rt"></i></span>';
		return '<li ' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
	}else {
// 		if($element['#original_link']['depth'] != 1) {
// 			$output = l($element['#title'], $element['#href'], $element['#localized_options']);
// 			return '<li ' . drupal_attributes($element['#attributes']) . '>' . $output . '</li>';
// 		}else {
// 			$element['#localized_options']['attributes']['class'][] = 'catalog-menu__item__header';
// 			$output = l($element['#title'], $element['#href'], $element['#localized_options']);
// 			return '<div ' . drupal_attributes($element['#attributes']) . '>' . $output . "</div>\n";
// 		}
		$output = l($element['#title'], $element['#href'], $element['#localized_options']);
		return '<li>' . $output . '</li>';
	}
}

function localvet_menu_tree__main_menu_sub($variables) {
	return '<ul>' . $variables['tree'] . '</ul>';
}

function localvet_menu_tree__menu_footer_1($variables) {
	return '<ul class="footer-list">' . $variables['tree'] . '</ul>';
}

function localvet_menu_link__menu_footer_1($variables) {
	$element = $variables['element'];
	$sub_menu = '';
	if ($element['#below']) {
		$element['#below']['#theme_wrappers'][0] = 'menu_tree__menu_footer_1_sub';
		$sub_menu = drupal_render($element['#below']);
		$element['#localized_options']['html'] = TRUE;
		$output = '<span class="footer-list-tt">' . $element['#title'] . '</span>';
		return '<div class="footer-list-wrapp">' . $output . $sub_menu . "</div>\n";
	}else {
		$output = l($element['#title'], $element['#href'], $element['#localized_options']);
		return '<li>' . $output . '</li>';
	}
}

function localvet_menu_tree__menu_footer_1_sub($variables) {
	return '<ul class="footer-list">' . $variables['tree'] . '</ul>';
}

function localvet_menu_tree__menu_footer_2($variables) {
	return '<ul class="footer-list">' . $variables['tree'] . '</ul>';
}

function localvet_menu_tree__menu_footer_3($variables) {
	return '<ul class="footer-list">' . $variables['tree'] . '</ul>';
}

function localvet_menu_link__menu_footer_2($variables) {
	$element = $variables['element'];
	$output = l($element['#title'], $element['#href'], $element['#localized_options']);
	return '<li>' . $output . '</li>';
}

function localvet_menu_link__menu_footer_3($variables) {
	$element = $variables['element'];
	$output = l($element['#title'], $element['#href'], $element['#localized_options']);
	return '<li>' . $output . '</li>';
}

function localvet_menu_tree__menu_lk($variables) {
	return '<div class="cabinet-nav">' . $variables['tree'] . '</div>';
}

function localvet_menu_link__menu_lk($variables) {
	$element = $variables['element'];
	$output = '<div class="cabinet-item"><i ' . drupal_attributes($element['#localized_options']['attributes']) . '></i>' . l($element['#title'], $element['#href']) . '</div>';
	return $output;
}

function localvet_preprocess_entity(&$variables, $hook) {
	$function = 'localvet_preprocess_' . $variables['entity_type'];
	//dpm($function);
	if (function_exists($function)) {
		$function($variables, $hook);
	}
	//dpm($variables);
	$variables['base_path'] = base_path();
	$variables['directory'] = path_to_theme();
}

function shtory_preprocess_bean(&$variables, $hook) {
	if($variables['elements']['#bundle'] == 'text_block') {
		$variables['view_mode'] = $variables['elements']['#entity']->view_mode;
	}
}

function shtory_webform_element($variables) {
  // Ensure defaults.
  $variables['element'] += array(
    '#title_display' => 'before',
  );

  $element = $variables['element'];

  // All elements using this for display only are given the "display" type.
  if (isset($element['#format']) && $element['#format'] == 'html') {
    $type = 'display';
  }
  else {
    $type = (isset($element['#type']) && !in_array($element['#type'], array('markup', 'textfield', 'webform_email', 'webform_number'))) ? $element['#type'] : $element['#webform_component']['type'];
  }

  // Convert the parents array into a string, excluding the "submitted" wrapper.
  $nested_level = $element['#parents'][0] == 'submitted' ? 1 : 0;
  $parents = str_replace('_', '-', implode('--', array_slice($element['#parents'], $nested_level)));

  $wrapper_attributes = isset($element['#wrapper_attributes']) ? $element['#wrapper_attributes'] : array('class' => array());
  $wrapper_classes = array(
    'form-item',
    'webform-component',
    'webform-component-' . $type,
  );
  if (isset($element['#title_display']) && strcmp($element['#title_display'], 'inline') === 0) {
    $wrapper_classes[] = 'webform-container-inline';
  }
  $wrapper_attributes['class'] = array_merge($wrapper_classes, $wrapper_attributes['class']);
  $wrapper_attributes['id'] = 'webform-component-' . $parents;
  $output = '<div ' . drupal_attributes($wrapper_attributes) . '>' . "\n";

  // If #title_display is none, set it to invisible instead - none only used if
  // we have no title at all to use.
  if ($element['#title_display'] == 'none') {
    $variables['element']['#title_display'] = 'invisible';
    $element['#title_display'] = 'invisible';
    if (empty($element['#attributes']['title']) && !empty($element['#title'])) {
      $element['#attributes']['title'] = $element['#title'];
    }
  }
  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . _webform_filter_xss($element['#field_prefix']) . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . _webform_filter_xss($element['#field_suffix']) . '</span>' : '';

  switch ($element['#title_display']) {
    case 'inline':
    case 'before':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

	case 'invisible':
    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= ' <div class="description">' . $element['#description'] . "</div>\n";
  }

  $output .= "\n</div>\n";

  return $output;
}

function shtory_textarea($variables) {
	$element = $variables['element'];
	element_set_attributes($element, array('id', 'name', 'cols', 'rows'));
	_form_set_class($element, array('form-textarea'));

	$wrapper_attributes = array(
		'class' => array('form-textarea-wrapper'),
	);

	// Add resizable behavior.
	if (!empty($element['#resizable'])) {
		drupal_add_library('system', 'drupal.textarea');
		$wrapper_attributes['class'][] = 'resizable';
	}

	$output = '';//'<div' . drupal_attributes($wrapper_attributes) . '>';
	$output .= '<textarea' . drupal_attributes($element['#attributes']) . '>' . check_plain($element['#value']) . '</textarea>';
// 	$output .= '</div>';
	return $output;
}


function localvet_theme() {
	$items = array();
	
	$items['user_login'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'user-login-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['user_login_block'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'user-login',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['user_register_form'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'user-register',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['user_profile_form'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'user-profile-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['hybridauth_additional_info_form'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'hybridauth-additional-info-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['user_pass'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'user-pass',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['tiu_filter_form'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'tiu-filter-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['firms_filter_form'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'firms-filter-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['comment_form__node_firm'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'comment-node-firm-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['comment_form__node_article'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'comment-node-article-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['comment_form__node_faq'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'comment-node-article-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['firm_node_form'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'firm-node-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['product_node_form'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'product-node-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	$items['faq_node_form'] = array(
		'render element' => 'form',
		'path' => drupal_get_path('theme', 'localvet') . '/templates/form',
		'template' => 'faq-node-form',
		'preprocess functions' => array(
			'localvet_preprocess_form',
		),
	);
	return $items;
	
}

function localvet_preprocess_form(&$variables) {
	$variables['classes_array'] = $variables['attributes_array'] = $variables['title_attributes_array'] = $variables['content_attributes_array'] =  array();
}


// function bf_preprocess_field_collection_item(&$variables) {
// 	$element = $variables['elements'];
// 	if($element['#bundle'] == 'field_docs') {
// 		if(in_array($variables['field_file'][0]['filemime'], array('image/png', 'image/jpeg'))) {
// 			$variables['link_attributes'] = array('class' => array('doc-btn', 'image-popup'));
// 			$variables['link_text'] = t('Preview');
// 		}else {
// 			$variables['link_attributes'] = array('class' => array('doc-btn'), 'target' => '_blank');
// 			$variables['link_text'] = t('Download');
// 		}
// 		$variables['link_url'] = trim(render($variables['content']['field_file']));
// 	}
// 	//dpm($variables);
// }

// function bf_form_element($variables) {
// 	$element = &$variables['element'];
// 
// 	// This function is invoked as theme wrapper, but the rendered form element
// 	// may not necessarily have been processed by form_builder().
// 	$element += array(
// 		'#title_display' => 'before',
// 	);
// 
// 	// Add element #id for #type 'item'.
// 	if (isset($element['#markup']) && !empty($element['#id'])) {
// 		$attributes['id'] = $element['#id'];
// 	}
// 	// Add element's #type and #name as class to aid with JS/CSS selectors.
// 	$attributes['class'] = array('form-item');
// 	if (!empty($element['#type'])) {
// 		$attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
// 	}
// 	if (!empty($element['#name'])) {
// 		$attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
// 	}
// 	// Add a class for disabled elements to facilitate cross-browser styling.
// 	if (!empty($element['#attributes']['disabled'])) {
// 		$attributes['class'][] = 'form-disabled';
// 	}
// 	$attributes['class'][] = 'form-group';
// 	$output = '<div' . drupal_attributes($attributes) . '>' . "\n";
// 
// 	// If #title is not set, we don't display any label or required marker.
// 	if (!isset($element['#title'])) {
// 		$element['#title_display'] = 'none';
// 	}
// 	$prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
// 	$suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';
// 
// 	switch ($element['#title_display']) {
// 		case 'before':
// 		case 'invisible':
// 		$output .= ' ' . theme('form_element_label', $variables);
// 		$output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
// 		break;
// 
// 		case 'after':
// 		$output .= ' ' . $prefix . $element['#children'] . $suffix;
// 		$output .= ' ' . theme('form_element_label', $variables) . "\n";
// 		break;
// 
// 		case 'none':
// 		case 'attribute':
// 		// Output no label and no required marker, only the children.
// 		$output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
// 		break;
// 	}
// 
// 	if (!empty($element['#description'])) {
// 		$output .= '<div class="description">' . $element['#description'] . "</div>\n";
// 	}
// 
// 	$output .= "</div>\n";
// 
// 	return $output;
// }

function shtory_link_formatter_link_default($vars) {
  $link_options = $vars['element'];
  //dpm($vars);
  unset($link_options['title']);
  unset($link_options['url']);

  if (isset($link_options['attributes']['class'])) {
    $link_options['attributes']['class'] = array($link_options['attributes']['class']);
  }
  // Display a normal link if both title and URL are available.
  if (!empty($vars['element']['title']) && !empty($vars['element']['url'])) {
	return l($vars['element']['title'], $vars['element']['url'], $link_options);
  }
  // If only a title, display the title.
  elseif (!empty($vars['element']['title'])) {
		if($vars['field']['field_name'] == 'field_link_list') {
			return "<span>{$vars['element']['title']}</span>";
		}else {
			return $link_options['html'] ? $vars['element']['title'] : check_plain($vars['element']['title']);
		}
  }
  elseif (!empty($vars['element']['url'])) {
    return l($vars['element']['title'], $vars['element']['url'], $link_options);
  }
}

function localvet_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => '<span class="icon icon-arrow-lt-dark"></span>', 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => '<span class="icon icon-arrow-rt-dark"></span>', 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last »')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
//     if ($li_first) {
//       $items[] = array(
//         'class' => array('pager-first'),
//         'data' => $li_first,
//       );
//     }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'),
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current active'),
            'data' => "<a href='#'>{$i}</a>",
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'),
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'),
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'),
        'data' => $li_next,
      );
    }
//     if ($li_last) {
//       $items[] = array(
//         'class' => array('pager-last'),
//         'data' => $li_last,
//       );
//     }
    return theme('item_list', array(
      'items' => $items,
      'attributes' => array('class' => array('pager', 'pagination')),
    ));
  }
}

function localvet_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];

  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  // @todo l() cannot be used here, since it adds an 'active' class based on the
  //   path only (which is always the current path for pager links). Apparently,
  //   none of the pager links is active at any time - but it should still be
  //   possible to use l() here.
  // @see http://drupal.org/node/1410574
  $attributes['href'] = url($_GET['q'], array('query' => $query));
  return '<a' . drupal_attributes($attributes) . '>' . $text . '</a>';
}

function localvet_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  // Only output the list container and title, if there are any list items.
  // Check to see whether the block title exists before adding a header.
  // Empty headers are not semantic and present accessibility challenges.
  $output = '<div>';
  if (isset($title) && $title !== '') {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    $i = 0;
    foreach ($items as $item) {
      $attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}

function localvet_vud_widget_template_variables_alter(&$variables) {
	$variables['plugin']['path'] = 'sites/all/themes/localvet/vud-widget-updown';
	//dpm($variables);
}

function localvet_preprocess_user_profile(&$variables) {
	$account = $variables['elements']['#account'];
	$variables['theme_hook_suggestions'][] = 'user_profile__' . $variables['elements']['#view_mode'];
	$variables['user_name'] = $account->name;
	$variables['base_path'] = base_path();
	$variables['directory'] = path_to_theme();
}

function localvet_hierarchical_select_selects_container($variables) {
	$element = $variables['element'];
	//dpm($element);
// 	$element['dropbox_add']['#buttontype'] = 'button';
// 	$element['dropbox_add']['#attributes']['class'][] = 'btn btn-min-md';
// 	$element['dropbox_add']['#prefix'] = '<div class="form-group">';
// 	$element['dropbox_add']['#suffix'] = '</div>';
	$output = '';
	$output .= '<div class="hierarchical-select clearfix">';
	$output .= drupal_render_children($element);
	$output .= '</div>';
	return $output;
}

function localvet_hierarchical_select_dropbox_table($variables) {
	$element = $variables['element'];
	$output = '';

	$class = 'dropbox';
	if (form_get_error($element) === '') {
		$class .= ' error';
	}

	$title     = $element['title']['#value'];
	$separator = $element['separator']['#value'];
	$is_empty  = $element['is_empty']['#value'];

	$separator_html = '<span class="hierarchical-select-item-separator">' . $separator . '</span>';

	$output .= '<div class="' . $class . '">';

	if (!$is_empty) {
		$output .= '<span class="site-h5-tt s-tablet-tt-md tt-light">' . $title . '</span>';
		// Each lineage in the dropbox corresponds to an entry in the dropbox table.
		$lineage_count = count(element_children($element['lineages']));
		for ($x = 0; $x < $lineage_count; $x++) {
			$db_entry = $element['lineages']["lineage-$x"];
// 			$zebra = $db_entry['#zebra'];
// 			$first = $db_entry['#first'];
// 			$last  = $db_entry['#last'];
// 			// The deepest level is the number of child levels minus one. This "one"
// 			// is the element for the "Remove" checkbox.
			$deepest_level = count(element_children($db_entry)) - 1;
// 
			$output .= '<div class="add-form-field">';//'<tr class="dropbox-entry ' . $first . ' ' . $last . ' ' . $zebra . '">';
// 			$output .= '<td>';
			// Each item in a lineage is separated by the separator string.
			for ($depth = 0; $depth < $deepest_level; $depth++) {
				$output .= '<span class="add-field-item">' . drupal_render($db_entry[$depth]) . '</span>';;

// 				if ($depth < $deepest_level - 1) {
// 					$output .= $separator_html;
// 				}
			}
// 			$output .= '</td>';
// 			$db_entry['remove']['#buttontype'] = 'button';
// 			$db_entry['remove']['#attributes']['class'][] = 'Удалить';
			$output .= '<span class="dropbox-remove">' . drupal_render($db_entry['remove']) . '</span>';
			$output .= '</div>';//'</tr>';
		}
	}
	else {
// 		$output .= '<tr class="dropbox-entry first last dropbox-is-empty"><td>';
// 		$output .= t('Nothing has been selected.');
// 		$output .= '</td></tr>';
	}

// 	$output .= '</tbody>';
// 	$output .= '</table>';
	$output .= '</div>';

	return $output;
}

function localvet_field_multiple_value_form($variables) {
	$element = $variables['element'];
	$output = '';
	//dpm($element);
	if ($element['#cardinality'] > 1 || $element['#cardinality'] == FIELD_CARDINALITY_UNLIMITED) {
		$table_id = drupal_html_id($element['#field_name'] . '_values');
		$order_class = $element['#field_name'] . '-delta-order';
		$required = !empty($element['#required']) ? theme('form_required_marker', $variables) : '';

		$header = array(
			array(
				'data' => '<label>' . t('!title !required', array('!title' => $element['#title'], '!required' => $required)) . "</label>",
				'colspan' => 2,
				'class' => array('field-label'),
			),
			t('Order'),
		);
		$rows = array();

		// Sort items according to '_weight' (needed when the form comes back after
		// preview or failed validation)
		$items = array();
		foreach (element_children($element) as $key) {
			if ($key === 'add_more') {
				$add_more_button = &$element[$key];
			}
			else {
				$items[] = &$element[$key];
			}
		}
		usort($items, '_field_sort_items_value_helper');

		// Add the items as table rows.
		foreach ($items as $key => $item) {
			$item['_weight']['#attributes']['class'] = array($order_class);
			$delta_element = drupal_render($item['_weight']);
			$cells = array(
				array('data' => '', 'class' => array('field-multiple-drag')),
				drupal_render($item),
				array('data' => $delta_element, 'class' => array('delta-order')),
			);
			$rows[] = array(
				'data' => $cells,
				'class' => array('draggable'),
			);
		}

		$output = '<div class="form-item">';
		$output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id, 'class' => array('field-multiple-table'))));
		$output .= $element['#description'] ? '<div class="description">' . $element['#description'] . '</div>' : '';
		$output .= '<div class="clearfix">' . drupal_render($add_more_button) . '</div>';
		$output .= '</div>';

		drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
	}
	else {
		foreach (element_children($element) as $key) {
		$output .= drupal_render($element[$key]);
		}
	}

	return $output;
}

/**
 * Creates a simple text rows array from a field collections, to be used in a
 * field_preprocess function.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 *
 * @param $field_name
 *   The name of the field being altered.
 *
 * @param $field_array
 *   Array of fields to be turned into rows in the field collection.
 */
 
function rows_from_field_collection(&$vars, $field_name, $field_array) {
	$vars['rows'] = array();
	foreach($vars['element']['#items'] as $key => $item) {
		$entity_id = $item['value'];
		$entity = field_collection_item_load($entity_id);
		$wrapper = entity_metadata_wrapper('field_collection_item', $entity);
		$row = array();
		foreach($field_array as $field){
			$row[$field] = $wrapper->$field->value();
		}
		$vars['rows'][] = $row;
	}
}

// function localvet_preprocess_field(&$vars, $hook){
//   if ($vars['element']['#field_name'] == 'field_contacts') {
//     $vars['theme_hook_suggestions'][] = 'field__contacts_collected';
//  
//     $field_array = array('field_address', 'field_phone','field_work_time', 'field_map');
//     rows_from_field_collection($vars, 'field_contacts', $field_array);
//   }
// }

function localvet_css_alter(&$css) {

	$exclude = array(
// 		'modules/menu/menu.css' => TRUE,
// 		'modules/system/system.menus.css' => TRUE,
	);
	$css = array_diff_key($css, $exclude);
 
}