<?php if(!isset($form['title'])): ?>
	<div class="nav-ch-select select-max-wrapp">
		<?php print drupal_render($form['region']); ?>
	</div>
	<div class="nav-ch-select select-max-wrapp">
		<?php print drupal_render($form['town']); ?>
	</div>
	<div class="nav-ch-select select-max-wrapp">
		<?php print drupal_render($form['type']); ?>
	</div>

	<div class="form-actions">
		<?php print drupal_render($form['form_build_id']) ?>
		<?php print drupal_render($form['form_id']) ?>
		<?php print drupal_render($form['submit']) ?>
		<?php print drupal_render_children($form) ?>
	</div>
<?php else: ?>
	<div class="form-group-half">
		<div class="form-group form-group-min">
			<div class="nav-ch-select select-max-wrapp">
				<?php print drupal_render($form['region']); ?>
			</div>
		</div>
		<div class="form-group form-group-min">
			<div class="nav-ch-select select-max-wrapp">
				<?php print drupal_render($form['town']); ?>
			</div>
		</div>
	</div>
	<div class="form-group-half">
		<div class="form-group form-group-min">
			<div class="nav-ch-select select-max-wrapp">
				<?php print drupal_render($form['type']); ?>
			</div>
		</div>
		<div class="form-group form-group-min">
			<div class="nav-ch-select select-max-wrapp">
				<?php print drupal_render($form['product']); ?>
			</div>
		</div>
	</div>
	<div class="form-group form-group-min">
		<div class="nav-ch-select select-max-wrapp">
			<?php print drupal_render($form['title']); ?>
		</div>
	</div>
	<div class="form-actions">
		<?php print drupal_render($form['form_build_id']) ?>
		<?php print drupal_render($form['form_id']) ?>
		<?php print drupal_render($form['submit']) ?>
		<?php print drupal_render_children($form) ?>
	</div>
<?php endif; ?>