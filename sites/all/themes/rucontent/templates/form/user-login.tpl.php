<div class="inner-dark-bl">
	<div class="form-group-half">
		<div class="form-group form-group-min">
			<?php print render($form['name']); ?>
		</div>
		<div class="form-group form-group-min">
			<?php print render($form['pass']); ?>
		</div>
	</div>
	<?php print render($form['actions']); ?>
	<div class="enter-check">
		<?php print render($form['remember_me']); ?>
	</div>
</div>
<div class="soc-enter">
	<span class="soc-enter-tt">Войдите через социальные сети нажав на соответствующий значок:</span>
	<?php print render($form['hybridauth']); ?>
	<div class="soc-enter-reg">
		<a href="/user/register">Зарегистрируйтесь!</a>
		<span>и получите все возможности <br> нашего сайта</span>
	</div>
</div>
<?php print drupal_render_children($form); ?>