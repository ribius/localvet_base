<?php global $base_path; $directory = path_to_theme(); ?>
<?php if($provider_name == 'Facebook'): ?>
	<span class="<?php print $icon_pack_classes; ?> soc-e-fb" title="<?php print $provider_name; ?>">
		<img src="<?php print $base_path . $directory; ?>/img/icon-round-fb.png" alt="">
	</span>
<?php elseif($provider_name == 'Twitter'): ?>
	<span class="<?php print $icon_pack_classes; ?> soc-e-tw" title="<?php print $provider_name; ?>">
		<img src="<?php print $base_path . $directory; ?>/img/icon-round-tw.png" alt="">
	</span>
<?php elseif($provider_name == 'ВКонтакте'): ?>
	<span class="<?php print $icon_pack_classes; ?> soc-e-vk" title="<?php print $provider_name; ?>">
		<img src="<?php print $base_path . $directory; ?>/img/icon-round-vk.png" alt="">
	</span>
<?php endif; ?>