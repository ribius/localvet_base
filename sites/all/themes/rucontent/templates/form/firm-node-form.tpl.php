<?php //dpm($form['field_contacts']); ?>
<div class="inner-light-bl-wrapp">
	<div class="light-bl">
		<div class="add-wrapp">
		
			<div class="form-add-head">
				<div class="form-group-dark">
					<div class="form-group form-group-min">
						<?php print drupal_render($form['title']); ?>
					</div>
					<div class="form-group from-group-file">
						<?php print drupal_render($form['field_image']); ?>
					</div>
					<div class="upload-info">
						<span class="upload-tt">Максимальный размер файла: <b>5 МБ.</b></span>
						<span class="upload-tt">Разрешённые типы файлов: <b>png gif jpg jpeg.</b></span>
					</div>
				</div>
			</div>


			<div class="inner-dark-bl">
				<span class="site-h4-tt">Рубрики</span>
				<?php print drupal_render($form['field_rubric_firm']); ?>
			</div>


			<div class="inner-dark-bl">
				<span class="site-h4-tt">станция метро</span>
				<?php print drupal_render($form['field_undeground']); ?>
			</div>


			<div class="inner-dark-bl">
				<span class="site-h4-tt">контакты</span>
				<?php print drupal_render($form['field_contacts']); ?>
			</div>


			<div class="inner-dark-bl">
				<span class="site-h4-tt">сайт</span>
				<?php print drupal_render($form['field_site']); ?>
			</div>


			<div class="inner-dark-bl">
				<div class="form-group">
					<?php print drupal_render($form['field_email']); ?>
				</div>
				<?php print drupal_render($form['field_desc']); ?>
			</div>


			<div class="inner-dark-bl">
				<span class="site-h4-tt">Опции</span>
				<div class="form-group">
					<span class="form-t-min-info">Здесь вы можете добавить информацию о наличии или отсутствии популярных товаров или услуг, а также их стоимости. Оно отобразится в структурированной таблице в карточке фирмы.</span>
				</div>
				<?php print drupal_render($form['field_option']); ?>
			</div>


			<div class="form-add-bottom">
				<div class="form-add-att">
					<i class="icon icon-attention"></i>
					<div class="form-add-att-info">Любая организация может размещаться на этой позиции <a href="#">СОВЕРШЕННО БЕСПЛАТНО</a> и получить <a href="#">ТАКОЙ САЙТ</a></div>
				</div>
				<?php print drupal_render($form['actions']); ?>
			</div>
			
		</div>
	</div>
</div>
              
<?php print drupal_render_children($form) ?>