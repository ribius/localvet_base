<?php //dpm($form); ?>
<div class="inner-dark-bl">
	<div class="form-group">
		<?php print render($form['account']['mail']); ?>
	</div>
	<div class="form-group">
		<?php print drupal_render($form['picture']); ?>
	</div>
	<div class="form-group">
		<?php print drupal_render($form['field_post']); ?>
	</div>
	<?php if(isset($form['account']['current_pass'])): ?>
		<div class="form-group">
			<?php print drupal_render($form['account']['current_pass']); ?>
		</div>
	<?php endif; ?>
	<div class="form-group">
		<?php print drupal_render($form['account']['pass']); ?>
	</div>
	<div class="form-group">
		<?php print drupal_render($form['subscriptions']); ?>
	</div>
	<?php print render($form['actions']); ?>
	<?php print drupal_render_children($form); ?>
</div>