<?php //dpm($form); ?>
<div class="stars-rating">
	<?php print render($form['field_rate']); ?>
</div>
<div class="form-group form-group-dark field-not-radius">
	<?php print render($form['comment_body']); ?>
</div>
<div class="review-form-bottom">
	<?php print render($form['actions']); ?>
	<span class="form-ruls">Нажимая кнопку Отправить вы соглашаетесь с <a href="/polzovatelskoe-soglashenie" target="_blank">Пользовательским соглашением</a> и <a href="/pravila-razmeshcheniya-informacii" target="_blank">Правилами размещения информации</a>.</span>
</div>
<div class="element-hidden">
	<?php print drupal_render_children($form) ?>
</div>