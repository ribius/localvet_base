<?php foreach($rows as $row): ?>
	<div class="form-group form-group-min">
		<?php print render($row['field_address']); ?>
	</div>
	<div class="form-group-half">
		<div class="form-group">
			<?php print render($row['field_phone']); ?>
		</div>
		<div class="form-group">
			<?php print render($row['field_work_time']); ?>
		</div>
	</div>
	<div class="form-group">
		<div class="map-contacts-w">
			<?php print render($row['field_map']); ?>
		</div>
		<span class="form-t-min-info">После заполнения полей нажмите "Добавить"</span>
	</div>
<?php endforeach; ?>