<div class="inner-dark-bl">
	<div class="form-group">
		<?php print render($form['name']); ?>
	</div>
	<?php print render($form['actions']); ?>
	<?php print drupal_render_children($form); ?>
</div>