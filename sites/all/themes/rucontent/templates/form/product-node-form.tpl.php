<div class="inner-light-bl-wrapp">
	<div class="light-bl">
		<div class="add-wrapp">
		
			<div class="form-add-head">
				<div class="form-group-dark">
					<div class="form-group form-group-min">
						<?php print drupal_render($form['title']); ?>
					</div>
					<div class="form-group">
						<?php print drupal_render($form['field_firm']); ?>
					</div>
				</div>
			</div>

			<div class="inner-dark-bl">
				<span class="site-h4-tt">Рубрика</span>
				<?php print drupal_render($form['field_rubric_tiu']); ?>
			</div>
			
			<div class="inner-dark-bl">
				<div class="form-group">
					<?php print drupal_render($form['field_img']); ?>
				</div>
				<div class="upload-info">
					<span class="upload-tt">Максимальный размер файла: <b>5 МБ.</b></span>
					<span class="upload-tt">Разрешённые типы файлов: <b>png gif jpg jpeg.</b></span>
				</div>
			</div>


			<div class="inner-dark-bl">
				<div class="form-group">
					<?php print drupal_render($form['field_price']); ?>
				</div>
				<?php print drupal_render($form['field_short_desc']); ?>
			</div>


			<div class="inner-dark-bl">
				<?php print drupal_render($form['field_desc']); ?>
			</div>


			<div class="form-add-bottom">
				<div class="form-add-att">
					<i class="icon icon-attention"></i>
					<div class="form-add-att-info">Любая организация может размещаться на этой позиции <a href="#">СОВЕРШЕННО БЕСПЛАТНО</a> и получить <a href="#">ТАКОЙ САЙТ</a></div>
				</div>
				<?php print drupal_render($form['actions']); ?>
			</div>
			
		</div>
	</div>
</div>
              
<?php print drupal_render_children($form) ?>