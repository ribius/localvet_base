<!--<div class="inner-light-bl-wrapp">
	<div class="light-bl">-->
		<div class="inner-dark-bl">
			<div class="form-group">
				<?php print render($form['account']['name']); ?>
			</div>
			<div class="form-group">
				<?php print render($form['account']['mail']); ?>
			</div>
			<div class="form-group">
				<?php print render($form['field_organization']); ?>
			</div>
			<div class="form-group">
				<?php print render($form['field_author']); ?>
			</div>
			<div class="form-group">
				<?php print render($form['captcha']); ?>
			</div>
			<?php print render($form['actions']); ?>
			<span class="form-ruls">
				Нажимая кнопку Создать аккаунт вы соглашаетесь с <a href="/polzovatelskoe-soglashenie" target="_blank">Пользовательским соглашением</a> и <a href="/pravila-razmeshcheniya-informacii" target="_blank">Правилами размещения информации</a>.
			</span>
			<div class="soc-enter">
				<span class="soc-enter-tt">Или войдите через социальные сети нажав на соответствующий значок:</span>
				<?php print render($form['hybridauth']); ?>
			</div>
			<?php print drupal_render_children($form); ?>
		</div>
	<!--</div>
</div>-->