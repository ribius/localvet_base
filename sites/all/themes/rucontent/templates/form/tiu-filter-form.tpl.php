<div class="form-group form-group-min">
	<?php print render($form['title']); ?>
</div>
<div class="form-group-half">
	<div class="form-group form-group-min form-group-sm-t">
		<div class="select-max-wrapp">
			<?php print render($form['region']); ?>
		</div>
	</div>
	<div class="form-group form-group-min form-group-sm-t">
		<div class="select-max-wrapp">
			<?php print render($form['town']); ?>
		</div>
	</div>
	<div class="form-group form-group-min form-group-md-t">
		<div class="select-max-wrapp">
			<?php print render($form['product']); ?>
		</div>
	</div>
</div>
<div class="form-search-btns">
	<?php print render($form['submit']); ?>
	<a href="/tiu" class="btn-reset">Сбросить фильтр</a>
</div>
<?php print drupal_render_children($form) ?>