<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>
<?php global $user; ?>
<div class="inner-light-bl-wrapp">
	<div class="light-bl">
		<p>Задайте вопрос через форму ниже.</p>
		<div class="inner-dark-bl">
			<div class="form-group">
				<?php print drupal_render($form['title']); ?>
			</div>
			<div class="form-group">
				<?php print drupal_render($form['field_short_desc']); ?>
			</div>
			<div class="form-group">
				<?php print drupal_render($form['field_img']); ?>
			</div>
			<div class="upload-info">
				<span class="upload-tt">Максимальный размер файла: <b>5 МБ.</b></span>
				<span class="upload-tt">Разрешённые типы файлов: <b>png gif jpg jpeg.</b></span>
			</div>
			<?php if(isset($form['field_answer'])): ?>
				<div class="form-group">
					<?php print drupal_render($form['field_answer']); ?>
				</div>
			<?php endif; ?>
			<?php print drupal_render($form['actions']); ?>
			<span class="form-ruls">Нажимая кнопку Задать вопрос вы соглашаетесь с <a href="/polzovatelskoe-soglashenie" target="_blank">Пользовательским соглашением</a> и <a href="/pravila-razmeshcheniya-informacii" target="_blank">Правилами размещения информации</a>.</span>
		</div>
		<div class="form-add-att">
			<i class="icon icon-attention"></i>
			<div class="form-add-att-info"><p>Вопрос будет размещен на сайте.</p></div>
		</div>
		<?php if(!in_array('administrator', $user->roles)): ?>
			<div class="element-hidden">
				<?php print drupal_render_children($form) ?>
			</div>
		<?php else: ?>
			<?php print drupal_render_children($form) ?>
		<?php endif; ?>
	</div>
</div>
