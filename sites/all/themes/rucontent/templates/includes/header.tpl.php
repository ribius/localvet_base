<?php $detect = mobile_detect_get_object(); ?>
<header id="header" class="header <?php print ($detect->isMobile()) ? 'header-mobile' : 'header-desktop' ?>">
	<div class="container group">
		<div class="head-menu">
			<div class="menu-icon-wrap">
				<span class="menu-icon">
				<span></span>
				</span>
				<span class="menu-close"></span>
			</div>


			<div class="menu-wrapper-fixed">
				<div class="menu-open-wrapper">
				<!--<div class="head-location">
					<i class="icon icon-loc-pin"></i>
					<a href="#modalCity" class="head-loc-link open-modal">Москва</a>
				</div>-->

				<nav class="header-nav">
					<?php print render($main_menu); ?>
				</nav>
					<?php //if($args[0] != 'spravochnik'): ?>
						<div class="nav-choise-wrapp">
							<?php print render($firms_filter); ?>
						</div>
					<?php //endif; ?>

				<div class="h-nav-links">
					<?php if(!$logged_in): ?>
						<a href="<?php print ($args[0] != 'user') ? '#modalEnter' : url('user/login'); ?>" class="h-nav-link-icon <?php print ($args[0] != 'user') ? 'open-modal' : ''; ?>">
							<i class="icon icon-lock"></i>
							<span class="h-nav-link-text">войти</span>
						</a>
						<a href="/user/register" class="h-nav-link-icon">
							<i class="icon icon-key"></i>
							<span class="h-nav-link-text">регистрация</span>
						</a>
					<?php elseif($logged_in): ?>
						<a href="<?php print url('user/' . $user->uid); ?>" class="h-nav-link-icon">
							<i class="icon icon-user"></i>
							<span class="h-nav-link-text">мой кабинет</span>
						</a>
						<a href="<?php print url('user/logout'); ?>" class="h-nav-link-icon">
							<i class="icon icon-logout"></i>
							<span class="h-nav-link-text">выйти</span>
						</a>
					<?php endif; ?>
					<a href="<?php print ($logged_in) ? url('node/add/firm') : url('node/14253'); ?>" class="h-nav-link-btn">
						<i class="icon icon-house"></i>
						<span class="h-nav-link-text">добавить компанию</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<?php if($logged_in): ?>
		<div class="head-dialog hidden-mobile">
			<?php print block_render('block', 35); ?>
		</div>
	<?php else: ?>
		<div class="head-dialog hidden-mobile">
			<?php print block_render('block', 36); ?>
		</div>
	<?php endif; ?>


	<div class="head-search">
		<?php print block_render('search', 'form'); ?>
	</div>


	<div class="logo">
		<a href="<?php print $front_page; ?>">
			<img src="<?php print $logo; ?>" alt="">
		</a>
		<span class="logo-slogan hidden-devices">
			<?php print $site_slogan; ?>
		</span>
		</div>

	</div>
</header>

<?php if($detect->isMobile() && isset($node) && in_array($node->type, array('product', 'firm', 'article', 'news'))): ?>
	<div class="mobile-share">
		<div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,viber,whatsapp,telegram" data-counter=""></div>
	</div>
<?php endif; ?>