<?php

/**
 * @file
 * widget.tpl.php
 *
 * UpDown widget theme for Vote Up/Down
 */
?>
<div class="vud-widget vud-widget-updown" id="<?php print $id; ?>">
	<?php if ($show_links): ?>
		<?php if ($show_up_as_link): ?>
			<a href="<?php print $link_up; ?>" rel="nofollow" class="<?php print $link_class_up; ?> rate-down"></a>
		<?php endif; ?>
		<?php if ($show_down_as_link): ?>
			<a href="<?php print $link_down; ?>" rel="nofollow" class="<?php print $link_class_down; ?> rate-up"></a>
		<?php endif; ?>
	<?php endif; ?>
	<span class="rate-value value-plus"><?php print $unsigned_points; ?></span>
	<?php if ($show_reset): ?>
		<a href="<?php print $link_reset; ?>" rel="nofollow" class="<?php print $link_class_reset; ?>" title="<?php print $reset_long_text; ?>">
			<div class="<?php print $class_reset; ?>">
				<?php print $reset_short_text; ?>
			</div>
		</a>
	<?php endif; ?>
</div>
