<?php

/**
 * @file
 * Default theme implementation to provide an HTML container for comments.
 *
 * Available variables:
 * - $content: The array of content-related elements for the node. Use
 *   render($content) to print them all, or
 *   print a subset such as render($content['comment_form']).
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default value has the following:
 *   - comment-wrapper: The current template type, i.e., "theming hook".
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * The following variables are provided for contextual information.
 * - $node: Node object the comments are attached to.
 * The constants below the variables show the possible values and should be
 * used for comparison.
 * - $display_mode
 *   - COMMENT_MODE_FLAT
 *   - COMMENT_MODE_THREADED
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_comment_wrapper()
 *
 * @ingroup themeable
 */
?>
<div id="comments" class="<?php print $classes; ?> inner-light-bl-wrapp"<?php print $attributes; ?>>
	<?php if ($content['comments'] && $node->type != 'forum'): ?>
		<?php print render($title_prefix); ?>
		<span class="site-h1-tt s-tablet-tt-md">Отзывы:</span>
		<?php print render($title_suffix); ?>
	<?php endif; ?>
  
	<div class="light-bl">
		<div class="review-bl">
			<?php if(user_is_anonymous()): ?>
				<div class="review-head">
					<span>Чтобы оставить отзыв, пожалуйста,</span>
					<a href="#modalEnter" class="btn btn-sm open-modal">авторизируйтесь</a>
					<span>используя социальные сети.</span>
				</div>
			<?php endif; ?>
			<?php print render($content['comments']); ?>
			<?php if ($content['comment_form']): ?>
				<div class="review-form">
					<span class="site-h4-tt s-tablet-tt-lg tt-light">добавить отзыв:</span>
					<?php print render($content['comment_form']); ?>
				</div>
			<?php else: ?>
				<form action="#" class="review-form">
					<span class="site-h4-tt s-tablet-tt-lg tt-light">добавить отзыв:</span>
					<div class="stars-rating">
						<span class="rating-tt">Оценка:</span>
						<i class="icon icon-star-fill"></i>
						<i class="icon icon-star-fill"></i>
						<i class="icon icon-star-fill"></i>
						<i class="icon icon-star-half"></i>
						<i class="icon icon-star-empty"></i>
					</div>
					<div class="form-group form-group-dark field-not-radius">
						<textarea placeholder="Текст комментария..."></textarea>
					</div>
					<div class="review-form-bottom">
						<button class="btn btn-md" type="submit" disabled="true">отправить</button>
						<span class="form-ruls">Нажимая кнопку Отправить вы соглашаетесь с <a href="/polzovatelskoe-soglashenie" target="_blank">Пользовательским соглашением</a> и <a href="/pravila-razmeshcheniya-informacii" target="_blank">Правилами размещения информации</a>.</span>
					</div>
				</form>
			<?php endif; ?>
		</div>
	</div>
	
</div>
