<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<?php //dpm($view->args); ?>
<?php if($rows || $header): ?>
	<div class="<?php print $classes; ?> inner-light-bl-wrapp">
		<?php print render($title_prefix); ?>
		<span class="site-h1-tt s-tablet-tt-md"><?php print $view->get_title();  ?></span>
		<?php print render($title_suffix); ?>
		
		<div class="light-bl">
			<?php if ($rows || $header): ?>
				<div class="view-content products-wrapp">
					<?php print $rows; ?>
					<?php print $header; ?>
				</div>
				<?php $detect = mobile_detect_get_object(); ?>
				<?php $block_id = ($detect->isMobile()) ? 34 : 12; ?>
				<?php print block_render('block', $block_id); ?>
			<?php elseif ($empty): ?>
				<div class="view-empty">
					<?php print $empty; ?>
				</div>
			<?php endif; ?>
			
			<?php if ($more): ?>
				<?php print $more; ?>
			<?php endif; ?>
		</div>

	</div><?php /* class view */ ?>
<?php endif; ?>
