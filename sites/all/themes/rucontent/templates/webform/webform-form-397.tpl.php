<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 *
 * If a preview is enabled, these keys will be available on the preview page:
 * - $form['preview_message']: The preview message renderable.
 * - $form['preview']: A renderable representing the entire submission preview.
 */
?>
<?php //dpm($form); ?>
<div class="inner-dark-bl">
	<div class="form-group-half">
		<div class="form-group form-group-min">
			<?php print drupal_render($form['submitted']['name']); ?>
		</div>
		<div class="form-group form-group-min">
			<?php print drupal_render($form['submitted']['email']); ?>
		</div>
	</div>
	<div class="form-group">
		<?php print drupal_render($form['submitted']['msg']); ?>
	</div>
	<?php print drupal_render($form['actions']); ?>
	<span class="form-ruls"><?php print strip_tags(drupal_render($form['submitted']['polzovatelskoe_soglashenie']), '<a>'); ?></span>
</div>
<div class="element-hidden">
	<?php print drupal_render_children($form) ?>
</div>
