<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

	<?php print render($title_prefix); ?>
	<?php print render($title_suffix); ?>

	<div class="light-bl">
		<div class="company-main">
			<div class="c-main-head">
				<?php if(isset($content['field_image'])): ?>
					<span class="c-main-logo"><?php print render($content['field_image']); ?></span>
				<?php endif; ?>
				<div class="c-main-map">
					<?php print views_embed_view('map', 'block', $node->nid); ?>
				</div>
			</div>
			<?php if(isset($content['field_contacts'])): ?>
				<div class="c-main-contacs-wrapp">
					<?php $c = count($node->field_contacts[LANGUAGE_NONE]); foreach($node->field_contacts[LANGUAGE_NONE] as $key => $item): ?>
						<div class="c-main-contacts-item">
							<?php print render($content['field_contacts'][$key]); ?>
							<?php if($key+1 == $c): ?>
								<?php if(isset($content['field_site'])): ?>
									<div class="company-contacts-item">
										<i class="icon icon-web"></i>
										<span class="c-contacts-info">
											<?php if(isset($content['field_pay']) && render($content['field_pay']) != FALSE): ?>
												<?php print render($content['field_site']); ?>
											<?php else: ?>
												<?php $site = field_view_field('node', $node, 'field_site', array('label' => 'hidden', 'type' => 'link_host')); print render($site); ?>
											<?php endif; ?>
										</span>
									</div>
								<?php endif; ?>
								<?php if(isset($content['field_email'])): ?>
									<div class="company-contacts-item">
										<i class="icon icon-mail"></i>
										<span class="c-contacts-info"><?php print render($content['field_email']); ?></span>
									</div>
								<?php endif; ?>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			
			<?php $detect = mobile_detect_get_object(); ?>
			<?php $block_id = ($detect->isMobile()) ? 25 : 8; ?>
			<?php print block_render('block', $block_id); ?>

			<div class="c-main-about">
				<?php if(isset($content['field_desc'])): ?>
					<div class="inner-dark-bl">
						<span class="site-h3-tt s-tablet-tt-lg tt-light">о компании</span>
						<?php print render($content['field_desc']); ?>
					</div>
				<?php endif; ?>
				<?php if(isset($content['field_option'])): ?>
					<ul class="c-main-param-list">
						<?php print render($content['field_option']); ?>
					</ul>
				<?php endif; ?>
			</div>

			<?php print views_embed_view('tiu', 'block'); ?>
			
			<?php $block_id = ($detect->isMobile()) ? 26 : 9; ?>
			<?php print block_render('block', $block_id); ?>

			<div class="c-main-share">
				<div class="share-line">
					<span class="share-tt">Поделиться <span>в соцсетях</span>:</span>
					<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
					<script src="//yastatic.net/share2/share.js"></script>
					<div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,viber,whatsapp,telegram" data-counter=""></div>
				</div>
				<div class="share-links">
					<?php $cl = array('btn', 'btn-border', 'btn-light', 'btn-min'); ?>
					<?php print l('это ваша компания?', 'node/398', array('attributes' => array('class' => $cl))); ?>
					<?php print l('Сообщить об ошибке', 'node/397', array('query' => array('from' => $node->nid), 'attributes' => array('class' => $cl))); ?>
				</div>
			</div>
		</div>
	</div>
	
	<?php if(count($content['comments']) > 0): ?>
		<?php print render($content['comments']); ?>
	<?php else: ?>
		<div id="comments" class="inner-light-bl-wrapp">
			<span class="site-h1-tt s-tablet-tt-md">отзывы:</span>
			<div class="light-bl">
				<div class="review-bl">
					<div class="review-head">
						<span>Чтобы оставить отзыв, пожалуйста,</span>
						<a href="#modalEnter" class="btn btn-sm open-modal">авторизируйтесь</a>
						<span>используя социальные сети.</span>
					</div>
					<form action="#" class="review-form">
						<span class="site-h4-tt s-tablet-tt-lg tt-light">добавить отзыв:</span>
						<div class="stars-rating">
							<span class="rating-tt">Оценка:</span>
							<i class="icon icon-star-fill"></i>
							<i class="icon icon-star-fill"></i>
							<i class="icon icon-star-fill"></i>
							<i class="icon icon-star-half"></i>
							<i class="icon icon-star-empty"></i>
						</div>
						<div class="form-group form-group-dark field-not-radius">
							<textarea placeholder="Текст комментария..."></textarea>
						</div>
						<div class="review-form-bottom">
							<button class="btn btn-md" type="submit" disabled="true">отправить</button>
							<span class="form-ruls">Нажимая кнопку Отправить вы соглашаетесь с <a href="#">Пользовательским соглашением</a> и <a href="#">Правилами размещения информации</a>.</span>
						</div>
					</form>
				</div>
			</div>
			<div class="light-bl-bottom-info">Мнение редакции может не совпадать с мнением автора отзыва.</div>
		</div>
	<?php endif; ?>
	
	<?php $near_nids = _localvet_near_firms_get_firms($node->field_rubric_firm[LANGUAGE_NONE][1]['tid'], $node->field_firm_id[LANGUAGE_NONE][0]['value']); ?>
	<?php print views_embed_view('firms', 'block_2', implode('+', $near_nids)); ?>
	
	<?php $block_id = ($detect->isMobile()) ? 38 : 37; ?>
	<div class="light-bl">
		<?php print block_render('block', $block_id); ?>
	</div>

</div>
