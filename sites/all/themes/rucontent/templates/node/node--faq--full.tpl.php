<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php $author = user_load($uid); ?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> node-faq-full clearfix">

	<?php print render($title_prefix); ?>
	<?php print render($title_suffix); ?>

	<div class="article-inner-wrapp">
		<?php if(isset($content['field_img'])): ?>
			<div class="article-img">
				<?php //print render($content['field_img'][0]); ?>
				
				<?php if(isset($content['field_img'])): ?>
						<div class="owl-slider-thumb-wrapp">
							<div class="owl-slider-main owl-carousel">
								<?php print render($content['field_img']); ?>
							</div>

							<div class="owl-thumbs owl-carousel">
								<?php print render($content['field_img']); ?>
							</div>
						</div>
					<?php endif; ?>
			</div>
		<?php endif; ?>
		<div class="inner-light-bl-wrapp">
			<div class="light-bl ">
				<div class="article-inner-info">
					<div class="a-inner-about">
						<span class="a-inner-name"><?php print $author->name; ?></span>
						<?php if(isset($author->field_post[LANGUAGE_NONE])): ?>
							<span class="a-inner-position"><?php print $author->field_post[LANGUAGE_NONE][0]['value']; ?></span>
						<?php endif; ?>
					</div>
					<?php if(isset($content['field_short_desc'])): ?>
						<blockquote><?php print render($content['field_short_desc']); ?></blockquote>
					<?php endif; ?>
					<?php //$block_recomended = (isset($content['field_tags_faq'])) ? block_render('views', 'faq-block_2') : ''; print $block_recomended; ?>
					
					<?php if(isset($content['field_answer'])): ?>
						<p><?php print render($content['field_answer']); ?></p>
					<?php endif; ?>
					
					<?php print block_render('views', 'faq-block_1'); ?>
					<?php $detect = mobile_detect_get_object(); ?> 
					<?php $block_id = ($detect->isMobile()) ? 22 : 21; ?>
					<?php print block_render('block', $block_id); ?>
				</div>
				
				<div class="article-inner-bottom">
					<?php if(isset($content['field_author_answer'])): ?>
						<div class="article-inner-person">
							<?php print render($content['field_author_answer']); ?>
						</div>
					<?php endif; ?>
				
					<div class="c-main-share">
						<div class="share-line">
							<div class="rate-panel">
								<?php print render($content['field_rate_updown']); ?>
							</div>
							<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
							<script src="//yastatic.net/share2/share.js"></script>
							<div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,viber,whatsapp,telegram" data-counter=""></div>
						</div>
						<?php if(isset($content['field_tags_faq'])): ?>
							<div class="share-tags">
								<?php print render($content['field_tags_faq']); ?>
							</div>
						<?php endif; ?>
						<a href="<?php print url('taxonomy/term/' . $node->field_rubric_faq[LANGUAGE_NONE][0]['tid']); ?>" class="btn btn-light btn-min">вернуться в рубрику</a>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<?php if(count($content['comments']) > 0): ?>
		<?php print render($content['comments']); ?>
	<?php else: ?>
		<div class="inner-light-bl-wrapp">
			<span class="site-h1-tt s-tablet-tt-md">комментарии:</span>
			<div class="light-bl">
				<div class="review-bl">
					<div class="review-head">
						<span>Чтобы оставить отзыв, пожалуйста,</span>
						<a href="#modalEnter" class="btn btn-sm open-modal">авторизируйтесь</a>
						<span>используя социальные сети.</span>
					</div>
					<form action="#" class="review-form">
						<span class="site-h4-tt s-tablet-tt-lg tt-light">добавить комментарий:</span>
						<div class="form-group form-group-dark field-not-radius">
							<textarea placeholder="Текст комментария..."></textarea>
						</div>
						<div class="review-form-bottom">
							<button class="btn btn-md" type="submit" disabled="true">отправить</button>
							<span class="form-ruls">Нажимая кнопку Отправить вы соглашаетесь с <a href="#">Пользовательским соглашением</a> и <a href="#">Правилами размещения информации</a>.</span>
						</div>
					</form>
				</div>
			</div>
			<div class="light-bl-bottom-info">Мнение редакции может не совпадать с мнением автора отзыва.</div>
			<?php //$detect = mobile_detect_get_object(); ?>
			<?php $block_id = ($detect->isMobile()) ? 24 : 23; ?>
			<div class="light-bl">
				<?php print block_render('block', $block_id); ?>
			</div>
		</div>
	<?php endif; ?>

</div>
